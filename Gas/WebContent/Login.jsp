<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<title>login Page</title>
<style>
h1 {
	color: deeppink;
	text-align: center;
}

td {
	font-size: 130%;
	padding: 15px;
	font-family: "Times New Roman", Times, serif;
}

input {
	font-size: 130%;
}

button {
	font-size: 130%;
}
</style>
</head>
<body>
<form action="Login" method="post">
	<h1>Login</h1>
	<div class="control-label col-sm-12"
		style="background-color: darkorange">

		
			<div class="col-sm-12" align="center">
				<table id="loginform" cellspacing=20 cellpadding=20>
					<tr>
						<td style="color: yellow"><h2>USER NAME :</h2></td>
						<td><h3>
								<input type="text" id="id" name="UserName"
									placeholder="Enter user name" required="required">
							</h3></td>
						<br>
					</tr>
					<tr>
						<td style="color: yellow"><h2>PASSWORD :</h2></td>
						<td><h3>
								<input type="password" name="Password"
									placeholder="Enter password" required>
							</h3></td>
					</tr>

					<tr>
						<td colspan=5 align="center">
								<label><input type="radio" name="radio" value="User"
									selected>Customer</label> <label><input type="radio"
									name="radio" value="Admin">Distributor</label>
							</td>
					</tr>
					<tr>
						<td colspan=5 align="center"><button type="submit"
								style="background-color: fuchsia">Login</button></td>
					</tr>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
				</table>

			</div>
		

	</div>
</form>
</body>
</html>