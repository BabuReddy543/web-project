<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

.navbar {
	overflow: hidden;
	height: 40px;
	background-color: #333;
}

.navbar a {
	float: center;
	font-size: 40px;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

.dropdown {
	float: left;
	overflow: hidden;
}

.dropdown .dropbtn {
	font-size: 15px;
	border: none;
	outline: none;
	color: black;
	padding: 14px 16px;
	background-color: inherit;
	font-family: inherit;
	margin: 0;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.box {
	width: 700px;
	height: 70px;
	padding: 20px;
	margin: 100px auto;
	margin-top: auto;
	background-position: center;
	box-shadow: 0px grey;
	border: 10px;
}

.mySlides {
	display: none;
	margin-left: initial;
	margin-right: inherit;
	height: 675px;
}

.dropdown-content a {
	float: none;
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
	text-align: left;
}

h3 {
	color: yellow;
}
</style>
</head>
<body>
	<form action="RegistrationServlet" method="post">
		<div class="navbar" style="background-color: deeppink">
			<center>
				<a href="#welcome">Welcome to Registration </a>
			</center>
		</div>
		<div class="control-label col-sm-4">
			<img class="mySlides" src="D:\images\image.jpg" style="width: 100%"
				height="100%" align="right"> <img class="mySlides"
				src="D:\images\image7.jpg" style="width: 100%" height="100%"
				align="right"> <img class="mySlides"
				src="D:\images\image00.jpg" style="width: 100%" height="100%"
				align="right">
		</div>
		<div class="control-label col-sm-8"
			style="background-color: darkorange" align="top">
			<br><br>
			<center>
				<h3>Consumer Details</h3>
			</center>
			<br><br>
			<div class="control-label col-sm-10">
				<table id="loginform" cellspacing=20 cellpadding=20>
					<div class="form-vertical">
						<div class="form-group">
							<label class="control-label col-sm-5" for="User Name">User
								Name:</label>
							<div class="col-sm-5">
								<left>
								<input type="User Name" class="form-control" id="User Name"
									placeholder="User Name" name="UserName">
							</div>
							<br>
						</div>
						<br>

						<div class="form-group">
							<label class="control-label col-sm-5" for="Email">Email:</label>
							<div class="col-sm-5">
								<input type="Email" class="form-control" id="Email"
									placeholder="Email" name="Email">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<label class="control-label col-sm-5" for="Password"><b>Password:</b></label>
							<div class="col-sm-5">
								<input type="password" class="form-control" id="Password"
									placeholder="Password" name="Password">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<label class="control-label col-sm-5" for="number">Phone
								Number:</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" id="number"
									placeholder=" Phone Number" name="PhoneNumber">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<label class="control-label col-sm-5" for="number">Consumer
								Number:</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" id="number"
									placeholder="Consumer Number" name="ConsumerNumber">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<label class="control-label col-sm-5" for="address">Address:</label>
							<div class="col-sm-5">
								<input type="Address" class="form-control" id="address"
									placeholder="Address" name="Address">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<label class="control-label col-sm-5" for="date">Created
								on:</label>
							<div class="col-sm-5">
								<input type="datetime-local" class="form-control"
									placeholder="Date" name="createdDate">
							</div>
							<br>
						</div>
						<br>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-5">
								<button type="Submit" class="btn-default"
									style="background-color: fuchsia">Register</button>
									<br> <br> <br> <br> <br> <br> <br>
							</div>
						</div>
					</div>
				</table>
			</div>
		</div>
	</form>
</body>
<script>
      var myIndex = 0;
      carousel();
      function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
          x[i].style.display = "none";  
        }
        myIndex++;
        if (myIndex > x.length) {myIndex = 1}    
        x[myIndex-1].style.display = "block";  
        setTimeout(carousel, 3000); // Change image every 3 seconds
      }
   </script>
</html>	
