
package serviceLogic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Bean.Registrationbean;

import Service.Dbconnection;


public class RegistrationLogic{

 

	public static int save(Registrationbean b){
		
		try {
			Connection con=Dbconnection.getConnection(); //getting the connection method here from dbconnection
			PreparedStatement ps = con.prepareStatement("insert into Customer values(CustomerSeq.nextval,?,?,?,?,?,?,?)");
			ps.setString(1, b.getUserName());//sending up the values received from user to the database table
			ps.setLong(2, b.getConNumber());
			ps.setString(3, b.getAddress());
			ps.setInt(4, b.getphnnmbr());
			ps.setString(5, b.getEmail());
			ps.setString(6, b.getPassword());
			ps.setDate(7, (Date) b.getcrDate());
			
			 int flag=ps.executeUpdate(); //value changes if it is executed
			if(flag!=0)
			{
				System.out.println("inserted");
			}
			con.close();
		} catch (SQLException e ) {
			
		System.out.println(e.getMessage());
		}
		return 0;
		
		
	}
	
}