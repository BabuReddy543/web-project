package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.Registrationbean;
import serviceLogic.RegistrationLogic;


/**
 * Servlet implementation class Registration
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		try {
			System.out.println("in post");
			String UserName=request.getParameter("UserName");	
			System.out.println("cons no " + request.getParameter("ConsumerNumber"));
			String conNumber = request.getParameter("ConsumerNumber");
			int ConsumerNumber=Integer.parseInt(conNumber);
			System.out.println("in" + ConsumerNumber);
			String Address=request.getParameter("Address");
			String phnnmbr=request.getParameter("PhoneNumber");
			int PhoneNumber=Integer.parseInt(phnnmbr);
			String Email=request.getParameter("Email");  
			String Password=request.getParameter("Password");
			String createdDate=request.getParameter("createdDate");
			
			
			Registrationbean b = new Registrationbean();
			b.setUserName(UserName);		//setting up the received values from index.jsp to setters and getters
			b.setConsumerNumber(conNumber);		
			
			b.setAddress(Address);
			b.setphnnmbr(PhoneNumber);		
			b.setEmail(Email);
			b.setPassword(Password);
			
			Date crDate  = Date.valueOf(createdDate);
			
			b.setcrDate(crDate);
		
					
			int checkFlag = RegistrationLogic.save(b); //sending all the values of pojo reference to save method in ServiceLogic
			System.out.println("FLAG " + checkFlag);
			
			if(checkFlag!=0){
				//out.print("SuccessFully Registered"); // if successfully executes save method
				PrintWriter out1 = response.getWriter();
				out1.printf("UserName ",UserName);
				out1.printf("ConsumerNumber", ConsumerNumber );
				out1.printf("Address ",Address);
				out1.printf("PhoneNumber", PhoneNumber );
				out1.printf("Email ",Email);
				out1.printf("Password", Password);
				out1.printf("crDate",crDate);
				
			}else{
				request.getRequestDispatcher("Homepage.html").include(request, response);

			}
		} catch (Exception e) { // will throw an exception if at all user typed any language constraints.
			System.out.println(e.getMessage());
			//out.print("<p align='center'>Please enter Valid Details</p>");
			request.getRequestDispatcher("Registration.jsp").include(request, response);
		}		
	}
}